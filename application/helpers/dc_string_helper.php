<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 生成翻页码
 * @param   string       $string     字符串
 * 
 * @return  string
 */
if ( ! function_exists('pager'))
{
	/**
	 * 去掉所有空格，含回车换行等
	 * @param   string
	 *
	 * @return  string
	 */
	function mv_bank($string)
	{
		return preg_replace("/\s+/", "", $string);
	}
}