<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 验证是否登录
 * @return  void 如果没登录，转到登录页面，如果已经登录返回session
 */
if ( ! function_exists('islogin'))
{
	function islogin()
	{
		$CI =& get_instance();
		$user = $CI->session->all_userdata();
		if(empty($user['u_id']))
		{
			redirect('home');
		}
	}
}