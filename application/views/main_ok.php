<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>提交站点</title>
<link href="/skin/my/style/main.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://libs.baidu.com/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	var time = 3;
	function time_back()
	{
		$('#timer').text(time);
		if(time==0)
		{
		  location.href='<?=$_SERVER['HTTP_REFERER']?>';
		} else {
		  time--;
		}
	}
	setInterval(time_back, 1000);
});

function apache_reload()
{
	$.get('<?=site_url('main/ajax_apache_reload')?>', function(data){
      if(data=='success')
	  {
	     $('#apache').html('apache重新载入配置成功<br />');
	  }
	})
}

</script>
</head>

<body>
<div class="wrap">
  <div class="message">
  <?
  if(!empty($shell_res))
  {
	  echo ($shell_res['ftpadd']=='success')    ? 'FTP配置成功<br />'   : (empty($shell_res['ftpadd']) ? '' : $shell_res['ftpadd'].'<br />');
	  echo ($shell_res['ftpdel']=='success')    ? 'FTP删除成功<br />'   : (empty($shell_res['ftpdel']) ? '' : $shell_res['ftpdel'].'<br />');
	  echo ($shell_res['mysqladd']=='success')  ? 'MYSQL配置成功<br />' : (empty($shell_res['mysqladd']) ? '' : $shell_res['mysqladd'].'<br />');
	  echo ($shell_res['mysqldel']=='success')  ? 'MYSQL数据库和对应用户删除成功<br />' : (empty($shell_res['mysqldel']) ? '' : $shell_res['mysqldel'].'<br />');	 
	  echo ($shell_res['mysqledit']=='success')  ? 'MYSQL用户密码修改成功<br />' : (empty($shell_res['mysqledit']) ? '' : $shell_res['mysqledit'].'<br />');
	  echo ($shell_res['siteadd']=='success')   ? '站点配置成功<br />'  : (empty($shell_res['siteadd']) ? '' : $shell_res['siteadd'].'<br />');
	  echo ($shell_res['sitedel']=='success')   ? '站点删除成功<br />'  : (empty($shell_res['sitedel']) ? '' : $shell_res['sitedel'].'<br />');
	  
	if($shell_res['httpd']=='1')
	{
  ?>
  <span id="apache"></span>
  <script type="text/javascript">apache_reload();</script>
  <?
	}	  
  }  
  ?>
  <?=empty($message) ? '' : $message.'<br />'?>
  <span id="timer">3</span>秒后将返回上一步，您可以选择下面的操作<br />
  <?
  switch($act)
  {
	  case 'add':
  ?>
    <a href="<?=site_url('main/add')?>">继续增加站点</a>
    <br />
    <a href="<?=site_url('main')?>">站点列表</a>
  <?
      break;
	  case 'edit':
  ?>
  <a href="<?=site_url('main/edit/'.$id)?>">重新编辑站点</a>
  <br />
  <a href="<?=site_url('main')?>">站点列表</a>
  <?
      break;
	  case 'del':
  ?>
  <a href="<?=$_SERVER['HTTP_REFERER']?>">返回站点列表</a>
  <?
	  break;
  }
  ?>  
  </div>
</div>
</body>
</html>