<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dc_shell extends CI_Controller
{
    /**
     * 构造函数
     *
     * @access  public
     * @return  void
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('islogin');
        islogin();

		//FTP全局管理员
		$this->ftp_root    = 'ftp_super';

		//数据库超级管理员
		$this->mysql_root  = 'root';

		//数据库超级管理员密码
		$this->mysql_root_pwd = 'root_passwd';

    }

	 /**
     * 统一调用操作服务器脚本
     *
     * @access  public
     * @return  void
     */
    public function allin($act, $params='')
    {
		if($act=='test')
		{
			print_r($params);
		}
		/*
		 * 增加站点
		 *
		 */
		if($act=='siteadd')
		{
            $params['flag'] = intval($params['flag']);
			$res = $this->site_add($params['dir'], $params['type'], $params['domain'], $params['port'], $params['flag']);
			return $res;
		}

		/*
		 * 删除站点
		 *
		 */
		if($act=='sitedel')
		{
			$res = $this->site_del($params['del_type'], $params['dir'], $params['type'], $params['domain'], $params['port']);
			return $res;
		}

		/* 修改域名绑定
		 *
		 * 未实际使用，方案：删除原网站域名绑定配置，然后直接增加新的配置(因为文件夹存在，将不新建)
		 *
		 */
		if($act=='siteedit')
		{
			$res = $this->site_edit($params['dir'], $params['old_type'], $params['type'], $params['old_domain'], $params['domain'], $params['old_port'], $params['port']);
			return $res;
		}


		/*
		 * 增加ftp
		 *
		 */
		if($act=='ftpadd')
		{
			$user        = $params['ftp_user'];
			$pwd         = $params['ftp_pwd'];
			$res         = $this->ftp_add($params['ftp_user'], $pwd, $params['dir']);
			return $res;
		}

		/*
		 * 删除ftp
		 *
		 */
		if($act=='ftpdel')
		{
			//设置删除ftp后的归属用户
			$s_user      = empty($params['s_ftp_user']) ? $this->ftp_root : $params['s_ftp_user'];
			$res         = $this->ftp_del($params['ftp_user'], $s_user, $params['dir']);
			return $res;
		}

		/*
		 * 修改ftp
		 * 没用上这个个函数，采用的方案：直接删除然后添加新的用户
		 */
		if($act=='ftpedit')
		{
			$user  = $params['user'];
			$pwd   = $params['pwd'];
			$res   = $this->ftp_edit($params['user'], $pwd);
			if(empty($res))
			{
				return 'success';
			}
		}

		/*
		 * 增加mysql数据库和对应用户
		 *
		 */
		if($act=='mysqladd')
		{
			$res   = $this->mysql_add($params['mysql_base'], $params['mysql_user'], $params['mysql_pwd']);
			return $res;

		}

		/*
		 * 删除mysql数据库和对于用户
		 *
		 */
		if($act=='mysqldel')
		{
			$res   = $this->mysql_del($params['mysql_base'], $params['mysql_user']);
			return $res;
		}

		/*
		 * 修改mysql用户密码
		 *
		 */
		if($act=='mysqledit')
		{
			$res   = $this->mysql_edit($params['mysql_user'], $params['mysql_pwd']);
			return trim($res);
		}

		/*
		 * 重新载入apache
		 *
		 *
		 */
		if($act=='apache_reload')
		{
			$res   = $this->apache_reload();
			if(empty($res))
			{
				return 'success';
			}
		}

	}


	/*==============以下为私有函数==============*/

    /**
     * 增加站点函数
     *
     * 调用增加站点sh脚本，成功返回success
     *
     * @dir      string 目录
     * @param $dir
     * @param $type
     * @param $domain
     * @param $port
     * @param int $flag
     * @internal param int $ 域名类型 1正式域名 2临时域名
     * @domian   string 域名
     * @port     int    端口 80 or 10010 ...
     * @port     flag   是否将站点设置成超级管理员所有，默认0为是
     * @return   string
     */
	private function site_add($dir, $type, $domain, $port, $flag=0)
	{
		$shell = "/usr/local/bin/siteadd $dir $type $domain $port $flag";
		return $this->web_exec($shell);
	}

	/**
	 * 删除站点函数
	 *
	 * 调用删除站点sh脚本，成功返回success
	 *
	 * @del_type int    删除类型 1仅删除域名绑定 2域名绑定文件夹目录都删除
	 * @dir      string 目录
	 * @type     int    域名类型 1正式域名 2临时域名
	 * @domian   string 域名
	 * @port     int    端口 80 or 10010 ...
	 * @return   string
	 */
	private function site_del($del_type, $dir, $type, $domain, $port)
	{
		//原表达式$ sed ':n;/ffff[^\n]*$/{$!N;$!N;d};/\n.*\n.*\n/!{N;bn};P;D' urfile
		$shell = "/usr/local/bin/sitedel $del_type $dir $type $domain $port";
		return $this->web_exec($shell);
	}


	/**
	 * 修改站点绑定域名
	 *
	 * 调用修改站点绑定域名sh脚本，成功返回success
	 *
	 * @dir          string 原目录目录
	 * @old_type     int    原域名类型 1正式域名 2临时域名
	 * @type         int    新域名类型 1正式域名 2临时域名
	 * @old_domian   string 原域名
	 * @domian       string 新域名
	 * @old_port     int    原端口 80 or 10010 ...
	 * @port         int    新端口 80 or 10010 ...
	 * @return       string
	 */
	private function site_edit($dir, $old_type, $type, $old_domain, $domain, $old_port, $port)
	{
		//先删除域名绑定
		$this->site_del(1, $dir, $old_type, $old_domain, $old_port);

		//再增加新的绑定绑定
		return $this->site_add($dir, $type, $domain, $port, 1);
	}


	/**
	 * 增加ftp函数
	 *
	 * 调用增加ftpsh脚本，成功返回success
	 *
	 * @user     string  用户名，直接用域名
	 * @pwd      string  密码，md5(域名+混淆字符串)
	 * @dir      string  目录，相对/www/web/而言，
	 * @return   string
	 */

	private function ftp_add($user, $pwd, $dir)
	{
		$shell = "/usr/local/bin/ftpadd $user $pwd $dir";
		return $this->web_exec($shell);
	}

	/**
	 * 删除ftp函数
	 *
	 * 直接调用命令，成功返回success
	 *
	 * @user     string  原用户名
	 * @s_user   string  全局ftp管理员
	 * @dir      string  目录，相对/www/web/而言
	 * @return   string
	 */
	private function ftp_del($user, $s_user, $dir)
	{
		$shell = "/usr/local/bin/ftpdel $user $s_user $dir";
		return $this->web_exec($shell);
	}

	/**
	 * 修改ftp密码函数
	 *
	 * 直接调用命令，成功返回success
	 *
	 * @user     string  原用户名
	 * @s_user   string  全局ftp管理员
	 * @dir      string  目录，相对/www/web/而言，
	 * @return   string
	 */
	private function ftp_edit($user, $pwd)
	{
		$shell = "echo $user:$pwd | chpasswd > /dev/null 2>&1";
		return $this->web_exec($shell);
	}


	/**
	 * 增加mysql数据库/用户函数
	 *
	 * 只用php采用root直接连接数据库操作，不用sh脚本，成功返回success
	 *
	 * @mysql_base   string  数据库名
	 * @mysql_user   string  用户名
	 * @mysql_pwd    string  密码
	 * @return       string
	 */
	private function mysql_add($mysql_base, $mysql_user, $mysql_pwd)
	{
		unset($this->db);
		$this->load->database('mysql://'.$this->mysql_root.':'.$this->mysql_root_pwd.'@localhost/mysql');
		$sql = "
		CREATE USER '$mysql_user'@'localhost' IDENTIFIED BY '$mysql_pwd';
		GRANT USAGE ON *.* TO '$mysql_user'@'localhost' IDENTIFIED BY '$mysql_pwd' WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;
		CREATE DATABASE IF NOT EXISTS `$mysql_base`;
		GRANT ALL PRIVILEGES ON `$mysql_base`.* TO '$mysql_user'@'localhost';";
		$sqls = explode(';', $sql);
		foreach($sqls as $v)
		{
			$this->db->query($v);
		}
		return 'success';
	}

	/**
	 * 删除mysql数据库/用户函数
	 *
	 * 只用php采用root直接连接数据库操作，不用sh脚本，成功返回success
	 *
	 * @mysql_base   string  数据库名
	 * @mysql_user   string  用户名
	 * @return       string
	 */
	private function mysql_del($mysql_base, $mysql_user)
	{
		unset($this->db);
		$this->load->database('mysql://'.$this->mysql_root.':'.$this->mysql_root_pwd.'@localhost/mysql');
		$sql = "
		DELETE FROM db WHERE Db='$mysql_base' AND User='$mysql_user';
		DELETE FROM user WHERE User='$mysql_user' AND Host='localhost';
		flush privileges;
		DROP DATABASE `$mysql_base`";
		$sqls = explode(';', $sql);
		foreach($sqls as $v)
		{
			$this->db->query($v);
		}
		return 'success';
	}


	/**
	 * 修改mysql用户密码
	 *
	 * 只用php采用root直接连接数据库操作，不用sh脚本，成功返回success
	 *
	 * @mysql_user   string 用户名
	 * @mysql_pwd    string  用户密码
	 * @return       string
	 */
	private function mysql_edit($mysql_user, $mysql_pwd)
	{
		unset($this->db);
		$this->load->database('mysql://'.$this->mysql_root.':'.$this->mysql_root_pwd.'@localhost/mysql');
		$sql = "SET PASSWORD FOR '$mysql_user'@'localhost' = PASSWORD( '$mysql_pwd' )";
		$this->db->query($sql);
		return 'success';
	}

	/**
	 * 重新载入apache配置
	 * 因为在php中直接调用php会导致网页无法打开，所以用ajax调用
	 *
	 */
	private function apache_reload()
	{
		$shell = "/usr/local/apache/bin/apachectl graceful";
		return $this->web_exec($shell);
	}


	/* 执行命令函数 */
	private function web_exec($shell)
	{
		$handle = popen("$shell", 'r');
		$read = fread($handle, 2096);
		pclose($handle);
		return trim($read);
	}
}